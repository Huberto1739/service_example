package com.edutec.services_example;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Switch;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Switch switch1 = (Switch) findViewById(R.id.switch1);
        if(PreferenceManager.checkPref(this,PreferenceManager.PREF_NOTIFICAR)) {
            switch1.setChecked(true);
        }
        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    PreferenceManager.setPref(MainActivity.this,PreferenceManager.PREF_NOTIFICAR,"1");
                }else{
                    PreferenceManager.delPref(MainActivity.this,PreferenceManager.PREF_NOTIFICAR);
                }
            }
        });
        Intent i = new Intent(this, Service.class);
        this.startService(i);
    }
}
