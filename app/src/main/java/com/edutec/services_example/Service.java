package com.edutec.services_example;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

/**
 * Created by Huberto on 25/01/17.
 */

public class Service extends android.app.Service {

    Service thisService;
    @Override
    public void onCreate() {
        super.onCreate();
        thisService=this;
        timer();
    }

    public Service(){}

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        timer();
        return null;
    }
    public static void showNotificationFromService(android.app.Service thisService, String title, String text, int icon,int id){
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(thisService)
                        .setSmallIcon(icon)
                        .setContentTitle(title)
                        .setContentText(text);
        Intent resultIntent = new Intent(thisService, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(thisService);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) thisService.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(id, mBuilder.build());
    }
    public static void cancelNotificationFromService(android.app.Service thisService){
        NotificationManager mNotificationManager =
                (NotificationManager) thisService.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancelAll();
    }
    public void timer(){
        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {}

            public void onFinish() {
                try{
                    if(PreferenceManager.checkPref(getBaseContext(),PreferenceManager.PREF_NOTIFICAR)) {
                        showNotificationFromService(thisService, "Recordatorio" ,"Esto es un recordatorio", R.drawable.ic_notif, 0);
                    }else{
                        cancelNotificationFromService(thisService);
                    }



                }catch (Exception e){
                    e.printStackTrace();
                }
                thisService.timer();
            }
        }.start();
    }
}
