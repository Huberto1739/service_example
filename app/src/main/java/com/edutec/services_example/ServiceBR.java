package com.edutec.services_example;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Huberto on 25/01/17.
 */

public class ServiceBR extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context,Service.class);
        context.startService(i);
    }
}
